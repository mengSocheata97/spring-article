
public class Main {
    String string;

    public Main(String string) {
        this.string = string;
    }

    public static void main(String[] args) {
        Main p = new Main("");
        //printing hello
        Hello hello = new Hello(p);
        hello.run();
        try {
            hello.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //printing star
        Star star = new Star(p);
        star.start();
        try {
            star.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Printing Sentence
        Sentence sentence = new Sentence(p);
        sentence.start();
        try {
            sentence.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //printing ------
        Minus minus = new Minus(p);
        minus.start();
        try {
            minus.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //printing sentence2
        Sentence2 sentence2 = new Sentence2(p);
        sentence2.start();
        try {
            sentence2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

//Thread for hello
class Hello extends Thread {
    private Main hello;

    public Hello(Main hello) {
        this.hello = hello;
    }

    public void run() {
        String h = "Hello KSHRD!";
        try {
            for (int i = 0; i < h.length(); i++) {
                System.out.print(h.charAt(i));
                Thread.sleep(250);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//Thread for star
class Star extends Thread {
    private Main star;

    public Star(Main star) {
        this.star = star;
    }

    public void run() {
        System.out.println();
        try {
            for (int i = 1; i < 40; i++) {
                System.out.print("*");
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//Thread Sentence
class Sentence extends Thread {
    private Main sentence;

    public Sentence(Main sentence) {
        this.sentence = sentence;
    }

    public void run() {
        String sen = "I will try my best to stay here at HRD.";
        try {
            System.out.println();
            for (int i = 0; i < sen.length(); i++) {
                System.out.print(sen.charAt(i));
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Minus extends Thread {
    private Main star;

    public Minus(Main star) {
        this.star = star;
    }

    public void run() {
        System.out.println();
        try {
            for (int i = 1; i < 40; i++) {
                System.out.print("-");
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//Thread Sentence2
class Sentence2 extends Thread {
    private Main sentence;

    public Sentence2(Main sentence) {
        this.sentence = sentence;
    }

    public void run() {
        System.out.println();
        System.out.print("Downloading");
        try {
            for (int i = 0; i < 9; i++) {
                System.out.print(".");
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("Completed 100%");
    }
}