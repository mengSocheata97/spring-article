package com.springhomework.demo.service;

import com.springhomework.demo.model.Category;

import java.util.List;

public interface CategoryService
{
    void addCategory(Category category);
    Category deleteCategory(int id);
    void updateCategory(Category category);
    List<Category> findAllCat();
    Category findByIdCat(int id);
}
