package com.springhomework.demo.repository.Provider;

import com.springhomework.demo.model.Article;
import com.springhomework.demo.model.Category;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

public class ArticleProvider
{
    public String update(Article article)
    {
        return new SQL(){{
            UPDATE("tb_article");
            SET("title ='"+article.getTitle()+"'");
            SET("description ='"+article.getDescription()+"'");
            SET("author ='"+article.getAuthor()+"'");
            SET("created_date ='"+article.getCreateDate()+"'");
            SET("cate_ID ='"+article.getCateID()+"'");
            SET("thumbnail='"+article.getThumbnail()+"'");
            WHERE("id ="+article.getId());
        }}.toString();
    }
    public String delete(Integer id)
    {
        return new SQL(){{
            DELETE_FROM("tb_article");
            WHERE("id ="+id);
        }}.toString();
    }
    public String findAll(Integer id)
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_article");
            if(id!=null)
            {
                WHERE("id ="+id);
            }
            ORDER_BY("id");
        }}.toString();
    }
    public String insert(Article article)
    {
        return new SQL(){{
            INSERT_INTO("tb_article");
            VALUES("title","'"+article.getTitle()+"'");
            VALUES("description","'"+article.getDescription()+"'");
            VALUES("author","'"+article.getAuthor()+"'");
            VALUES("thumbnail","'"+article.getThumbnail()+"'");
            VALUES("cate_id","'"+article.getCateID()+"'");
            VALUES("created_date","'"+article.getCreateDate()+"'");

        }}.toString();
    }

    public String search(String keyword)
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_article");
            WHERE("title ILIKE '%"+keyword+"%'");
            OR().WHERE("author ILIKE '%"+keyword+"%'");
            ORDER_BY("id");
        }}.toString();
    }

    public String getPage(int limit,int offset){
        return new SQL(){{
            SELECT("*");
            FROM("tb_article");
            ORDER_BY("id LIMIT #{limit} OFFSET #{offset}");
        }}.toString();
    }
}
