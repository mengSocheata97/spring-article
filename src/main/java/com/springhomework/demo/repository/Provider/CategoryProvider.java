package com.springhomework.demo.repository.Provider;

import com.springhomework.demo.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider
{
    public String findAllCat(Integer id)
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");
            if(id!=null)
                WHERE("id=#{id}");
            ORDER_BY("id");
        }}.toString();
    }

    public String addCategory(Category category)
    {
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("type","'"+category.getType()+"'");
        }}.toString();
    }
    public String updateCategory(Category category)
    {
        return new SQL(){
            {
                UPDATE("tb_category");
                SET("type ='" + category.getType() + "'");
                WHERE("id ="+category.getId());
            }}.toString();
    }

    public String deleteCategory(Integer id)
    {
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("id ="+id);
        }}.toString();
    }
}
