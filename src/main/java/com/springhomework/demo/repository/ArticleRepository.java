package com.springhomework.demo.repository;

import com.springhomework.demo.model.Article;
import com.springhomework.demo.model.Category;
import com.springhomework.demo.repository.Provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {
    @SelectProvider(method = "findAll",type = ArticleProvider.class)
    @Results({
            @Result(property = "createDate",column = "created_date"),
            @Result(property = "cateID",column = "cate_id")}
    )
    List<Article> findAll();

    @SelectProvider(method = "findAll",type = ArticleProvider.class)
    @Results({
            @Result(property = "createDate",column = "created_date"),
            @Result(property = "cateID",column = "cate_id") }
    )
    Article findById(int id);


    @SelectProvider(method = "delete",type=ArticleProvider.class)
    void deleteById(int id);

    @SelectProvider(method = "update",type = ArticleProvider.class)
    void update(Article article);

    @Insert("insert into tb_article (title,description,author,thumbnail,created_date) values (#{title},#{description},#{author},#{thumbnail},#{createDate})")
    @Options(useGeneratedKeys = true)
    void saveArticle(Article article);

    @SelectProvider(method = "search",type = ArticleProvider.class)
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "cateID",column = "cate_id")
    })
    List<Article> search(String keyword);

    @SelectProvider(method = "getPage",type = ArticleProvider.class)
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "cateID",column = "cate_id")
    })
    List<Article> getPage(int limit, int offset);

}
