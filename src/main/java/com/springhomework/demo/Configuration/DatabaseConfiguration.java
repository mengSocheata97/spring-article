package com.springhomework.demo.Configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.springhomework.demo.repository")
public class DatabaseConfiguration {
    @Bean
    @Profile("development")
    public DataSource development()
    {
        EmbeddedDatabaseBuilder emd = new EmbeddedDatabaseBuilder();
        emd.setType(EmbeddedDatabaseType.H2);
        emd.addScript("sql/table.sql");
        return  emd.build();
    }
}
