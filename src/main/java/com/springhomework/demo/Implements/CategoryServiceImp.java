package com.springhomework.demo.Implements;

import com.springhomework.demo.model.Category;
import com.springhomework.demo.repository.CategoryRepository;
import com.springhomework.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService
{
    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryService(CategoryRepository categoryService) {
        this.categoryRepository = categoryService;
    }

    @Override
    public void addCategory(Category category) {
        categoryRepository.addCategory(category);
    }

    @Override
    public Category deleteCategory(int id) {
        return categoryRepository.deleteCategory(id);
    }

    @Override
    public void updateCategory(Category category) {
        categoryRepository.updateCategory(category);
    }

    @Override
    public List<Category> findAllCat() {
        return categoryRepository.findAllCat();
    }

    @Override
    public Category findByIdCat(int id) {
        return categoryRepository.findByIdCat(id);
    }

}
