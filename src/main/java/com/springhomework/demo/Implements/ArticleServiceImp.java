package com.springhomework.demo.Implements;

import com.springhomework.demo.model.Article;
import com.springhomework.demo.model.Category;
import com.springhomework.demo.repository.ArticleRepository;
import com.springhomework.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class ArticleServiceImp implements ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findById(int id) {
        return articleRepository.findById(id);
    }
    @Override
    public void delete(int id) {
        articleRepository.deleteById(id);
    }

    @Override
    public void update(Article article) {
        article.setCreateDate(new Date().toString());
        articleRepository.update(article);
    }

    @Override
    public void saveArticle(Article article) {
        articleRepository.saveArticle(article);
    }

    @Override
    public List<Article> search(String keyword) {
        return articleRepository.search(keyword);
    }

    @Override
    public List<Article> getPage(int limit, int offset) {
        return articleRepository.getPage(limit,offset);
    }

}
