package com.springhomework.demo.controller;

import com.springhomework.demo.model.Article;
import com.springhomework.demo.model.Category;
import com.springhomework.demo.service.ArticleService;
import com.springhomework.demo.service.CategoryService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMapping.*;

@Controller
public class HomeController {

    @Value(value = "${per}")
    int perPage;

    private CategoryService categoryService;
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    private ArticleService articleService;
    @Autowired
    public void setArticleService(ArticleService articleService)
    {
        this.articleService = articleService;
    }

    @GetMapping("/")
    public String home(ModelMap map)
    {
        List<Article> articleList = articleService.findAll();
        map.addAttribute("articles",articleService.getPage(perPage,0));

        map.addAttribute("pages",calPage(articleService.findAll().size(),perPage));
        map.addAttribute("active",1);

        articleList.forEach( System.out::println);
        return "index";
    }

    @GetMapping("/add")
    public String add(ModelMap map)
    {
        map.addAttribute("article",new Article());
        map.addAttribute("isAdd",true);
        System.out.println("Get Mapping add");
        return "add";
    }

    @GetMapping("/addCate")
    public String addCate(ModelMap map)
    {
        map.addAttribute("category",new Category());
        System.out.println("Get Mapping add");
        return "addCate";
    }

    @PostMapping("/addCate")
    public String insertArticle(@Valid @ModelAttribute Category category)
    {
        System.out.println("posting");
            System.out.println(category);
            categoryService.addCategory(category);
            System.out.println(category);
                return "redirect:/category";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id)
    {
        System.out.println("id"+id);
        articleService.delete(id);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String update(ModelMap map ,@PathVariable("id") Integer id)
    {
        map.addAttribute("article",articleService.findById(id));
        map.addAttribute("isAdd",false);


        return "update";
    }

    @PostMapping("/update")
    public String saveUpdate(@ModelAttribute Article article ,@RequestParam("file") MultipartFile file)
    {
        if(!file.isEmpty())
        {
            String nameFile= UUID.randomUUID().toString() +file.getOriginalFilename();
            try {
                Files.copy(file.getInputStream(), Paths.get("src\\main\\resources\\image" ,nameFile));

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(article);
            article.setThumbnail(nameFile);
            articleService.update(article);
            System.out.println(article);

        }
        return "redirect:/";
    }

    @GetMapping("/view/{id}")
    public String viewForm(ModelMap map,@PathVariable("id") Integer id)
    {
        map.addAttribute("article",articleService.findById(id));
        return "view";
    }

    @GetMapping("/search")
    public String search(@RequestParam(value = "search",required = false, defaultValue = "cheata") String search, Model m)
    {
        System.out.println(articleService.search(search).size());
        m.addAttribute("pages",calPage(articleService.search(search).size(),perPage));
        m.addAttribute("active",1);
        m.addAttribute("articles",articleService.search(search));
        return "index";
    }

    // pagination link
    @GetMapping("/p/{p}")
    public String pagination(@PathVariable("p") Integer p, Model model){

        int offset = (p ==1 || p == 0) ? 0 : (p * perPage)- perPage;

        model.addAttribute("pages",calPage(articleService.findAll().size(),perPage));
        model.addAttribute("articles",articleService.getPage(perPage,offset));
        model.addAttribute("active",p);

        return "index";
    }

    // pagination
    private int calPage(int total, int n){
        if(total<n) total=n;
        double d = (double) total/ (double)n;
        return Math.abs((int)Math.ceil(d));
    }

    //    this is category
    @GetMapping("/category")
    public String homeCategory(ModelMap map)
    {
        System.out.println("HOME CATEGORY...");

        map.addAttribute("category",categoryService.findAllCat());

        System.out.println(categoryService.findAllCat().size());

        return "category";
    }
}
